<section class="contact py-5" id="contact">
    <div class="container">
        <h1 class="pb-5 pt-3">Entre em contato</h1>
        <div class="row d-none d-md-flex pt-5">
            <div class="col-12 col-md-5 col-lg-6 col-xxl-7 order-1 order-md-0">
                <img src="{{ asset('images/min/contacts.svg') }}" class="img-fluid" alt="contatos">
            </div>
            <div class="col-12 col-md-7 col-lg-6 col-xxl-5 order-0 order-md-1">
                <form class="d-flex justify-content-center bg-white" method="post" action="{{ route('sendContacts') }}">
                    @csrf
                    <div class="card border-0">
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="text" placeholder="Nome Completo" value="{{ old('name') }}" name="name">
                            <label for="floatingInput">Nome Completo</label>
                            <x-error field="name"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="tel" placeholder="Telefone" value="{{ old('phone') }}" name="phone">
                            <label for="floatingInput">Telefone</label>
                            <x-error field="phone"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="email" placeholder="E-mail" value="{{ old('email') }}" name="email">
                            <label for="floatingInput">E-mail</label>
                            <x-error field="email"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <textarea class="form-control border-0 border-bottom" placeholder="Mensagem" name="message" rows="10">{{ old('message') }}</textarea>
                            <label for="floatingInput">Mensagem</label>
                            <x-error field="message"  class="text-danger"/>
                        </div>
                        <div class="form-group pt-4 pb-md-4 pb-lg-5 d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary text-uppercase px-5">enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container d-md-none">
        <div class="row">
            <div class="col-12">
                <form class="d-flex justify-content-center bg-white">
                    <div class="card border-0">
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="text" placeholder="Nome Completo" value="{{ old('name') }}" name="name">
                            <label for="floatingInput">Nome Completo</label>
                            <x-error field="name"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="tel" placeholder="Telefone" value="{{ old('phone') }}" name="phone">
                            <label for="floatingInput">Telefone</label>
                            <x-error field="phone"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <input class="form-control border-0 border-bottom" type="email" placeholder="E-mail" value="{{ old('email') }}" name="email">
                            <label for="floatingInput">E-mail</label>
                            <x-error field="email"  class="text-danger"/>
                        </div>
                        <div class="form-floating">
                            <textarea class="form-control border-0 border-bottom" placeholder="Mensagem" name="message" rows="10">{{ old('message') }}</textarea>
                            <label for="floatingInput">Mensagem</label>
                            <x-error field="message"  class="text-danger"/>
                        </div>
                        <div class="form-group py-4 d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary text-uppercase px-5">enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="contact-bg"></div>
    <div class="container">
        <div class="row d-md-none">
            <div class="col-12">
                <img src="{{ asset('images/min/contacts.svg') }}" class="img-fluid" alt="contatos">
            </div>
        </div>
    </div>
</section>