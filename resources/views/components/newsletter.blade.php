<section class="newsletter py-5">
    <div class="container">
        <ul class="nav justify-content-between">
            <li>
                <h1 class="pb-3">Fique por dentro de nossas novidades</h1>
            </li>
            <li>
               <form class="row d-flex justify-content-center" method="post" action="{{ route('sendNewslleters') }}">
                    @csrf
                    <input type="text" class="form-control text-center border-0" name="email" placeholder="Digite aqui seu e-mail" aria-label="First name">
                    <button type="submit" class="btn btn-warning d-flex justify-content-center align-items-center mt-2">></button>
                </form>
            </li>
        </ul>
    </div>
</section>