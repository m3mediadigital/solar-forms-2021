<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Benefits;
use App\Models\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BenefitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.blog.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = new Files;

        try {
            $file->upload($file, $request->file);
        } catch (\Throwable $th) {
            // throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao cadastrar imagem!');
        }

        $request['files_id'] = $file->id;
        $request['users_id'] = auth()->user()->id;

        try {
            Benefits::create($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao cadastrar vatagem!');
        }

        return redirect()->route('dashboard.benefits.index')->with('success','Vantagem Cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Benefits  $benefits
     * @return \Illuminate\Http\Response
     */
    public function show(Benefits $benefits)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Benefits  $benefits
     * @return \Illuminate\Http\Response
     */
    public function edit(Benefits $benefits)
    {
        return view('dashboard.benefits.edit',['item' => $benefits]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Benefits  $benefits
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benefits $benefits)
    {
        if($request->file):
            $file = new Files;

            try {
                $file->upload($file, $request->file);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error','Erro ao atualizar imagem!');
            }

            $request['files_id'] = $file->id;

        endif;
        
        try {
            $benefits->update($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao atualizar vantagem!');
        }

        return redirect()->route('dashboard.benefits.index')->with('success','Vantagem atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Benefits  $benefits
     * @return \Illuminate\Http\Response
     */
    public function destroy(Benefits $benefits)
    {

        try {
            $benefits->delete();
        } catch (\Throwable $th) {
            // throw $th;
            Log::error($th);
            return redirect()->back()->with('error','Erro ao excluir blog!');
        }

        return redirect()->back()->with('success','Vantagem excluído com sucesso!');
    }
}
