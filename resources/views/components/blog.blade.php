<section class="blog pt-5" id="blog">
    <div class="container">
        <h1 class="text-capitalize text-center py-lg-5">notícias</h1>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card border-0 bg-neutral" style="background-image: url('{{ asset('images/min/blog.png') }}')">
                    <div class="card-body position-absolute bottom-0">
                        <h3 class="card-title">Card title</h3>
                        <p class="card-text pt-2 pb-3">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </p>
                    </div>
                </div>
            </div>            
            <div class="col-12 col-lg-6">
                @for ($i = 0; $i < 2; $i++)
                    <div class="card card-more border-0 pt-4">
                        <div class="card-body">
                            <h3 class="card-title">Card title</h3>
                            <p class="card-text pt-2 pb-3">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                            <div class="w-100 d-flex justify-content-center d-lg-block">
                                <a href="">Leia Mais</a>
                            </div>                
                        </div>
                    </div>
                @endfor
            </div>
        </div>    
    </div>
</section>