<div class="offcanvas offcanvas-start w-100" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header">
        <button type="button" class="btn-close text-white" data-bs-dismiss="offcanvas" aria-label="Close">X</button>
    </div>
    <div class="offcanvas-body d-flex justify-content-center align-items-center">
        <ul class="navbar-nav w-100">
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#beneficies">Benfícios</a>
            </li>
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#calculator">Calculadora</a>
            </li>
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#structure">Nossa estrutura</a>
            </li>
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#insight">Nossos clientes</a>
            </li>
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#blog">Notícias</a>
            </li>
            <li class="nav-item py-2">
                <a class="nav-link text-white text-center" data-bs-dismiss="offcanvas" aria-label="Close" href="#contact">Contato</a>
            </li>
            <li class="nav-item py-3 pt-4 d-flex justify-content-center">
                <a class="nav-link text-white text-center btn btn-warning text-uppercase w-50" data-bs-dismiss="offcanvas" aria-label="Close" href="#">acesso cliente</a>
            </li>
            <li class="nav-item py-3 pt-4 d-flex justify-content-center">
                <a class="nav-link text-white text-center btn btn-warning text-uppercase w-50" data-bs-dismiss="offcanvas" aria-label="Close" href="#">geração distribuida</a>
            </li>
        </ul>
    </div>
</div>