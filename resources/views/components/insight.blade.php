<section class="insight" id="insight">
    <img src="{{ asset('images/min/client-left.svg') }}" class="insight-left">
    <div class="container">
        <div class="col-12">
            <h1 class="text-capitalize py-3 pb-lg-5">nosso clientes</h1>
            <div class="owl-carousel owl-themer insight-carousel">
                @foreach ($items as $item)
                    <div class="item d-flex justify-content-center">
                        <img src="{{ $item->files->path }}" alt="{{ $item->title }}">
                    </div>
                @endforeach
            </div>
        </div>
        {{-- <div class="col-12 py-5">
            <h2 class="pt-2 pb-3 pt-lg-4">O que falam de nós</h2>
            <div class="owl-carousel owl-themer insight-carousel">
                @for ($i = 0; $i < 3; $i++)
                    <div class="item">
                        <div class="card w-100 border-0 bg-transparent">
                            <div class="card-header d-flex justify-content-center border-0 bg-transparent">
                                <img src="{{ asset('images/icons/fala.svg') }}" class="card-img-top img-fluid w-50" alt="...">
                            </div>
                            <div class="card-body pt-5">
                                <h5 class="card-title text-center pt-5">Card title</h5>
                                <h6 class="card-subtitle text-center mb-2">Card subtitle</h6>
                                <p class="card-text text-center pb-3">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div> --}}
    </div>
</section>