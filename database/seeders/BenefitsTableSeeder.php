<?php

namespace Database\Seeders;

use App\Models\Benefits;
use App\Models\Files;
use Illuminate\Database\Seeder;

class BenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'image' => 'images/icons/reducao.png',
                'title'=> 'Redução do custo de energia'
            ],
            [
                'image' => 'images/icons/ambiente.png',
                'title'=> 'Contribuição para o meio ambiente'
            ],
            [
                'image' => 'images/icons/solar.png',
                'title'=> 'Sem investimento em equipamentos solares'
            ],
            [
                'image' => 'images/icons/cache.png',
                'title'=> 'Cash Energy: 8,5% de desconto da sua fatura SOLAR FARMS'
            ],
            [
                'image' => 'images/icons/fatura.png',
                'title'=> 'Gestão e acompanhamento das faturas'
            ],
        ];

        foreach($items as $item){
            $file = new Files;
            $file->name = $item['image'];
            $file->filename = $item['image'];
            $file->path = $item['image'];
            $file->save();

            $bene = new Benefits;
            $bene->title = $item['title'];
            $bene->files_id = $file->id;
            $bene->save();
        }
    }
}
