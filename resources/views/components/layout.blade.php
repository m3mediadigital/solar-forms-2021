<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Safecomply -->
    <meta name="adopt-website-id" content="1d31473f-dcd0-4821-a90c-94f25a0a0d8e" />
    <script src="//tag.goadopt.io/injector.js?website_code=1d31473f-dcd0-4821-a90c-94f25a0a0d8e" class="adopt-injector">
    </script>

    {{ @$css }}    
</head>
<body class="bg-white">
    <x-Header />   
    <x-MobileMenu /> 
    {{ $content }}
    <x-Footer />
    <script src="{{ asset('js/app.js') }}"></script>
    {{ @$js }}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        @if(session('success'))
            Swal.fire(
                'Sucesso',
                '{!! session('success') !!}',
                'success'
                
            )
        @endif

        @if(session('error'))
            Swal.fire(
                'Erro!',
                '{!! session('error') !!}',
                'error',
            )
        @endif
    </script>
    
</body>
</html>