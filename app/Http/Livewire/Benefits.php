<?php

namespace App\Http\Livewire;

use App\Models\Benefits as ModelsBenefits;
use Livewire\Component;
use Livewire\WithPagination;

class Benefits extends Component
{
    use WithPagination;

    public $search;
    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.benefits',[
            'items' => ModelsBenefits::where('title', 'like', '%'.$this->search.'%')
                        ->orderBy('created_at','asc')
                            ->paginate(15)
        ]);
    }
}
