<section class="structure py-5" id="structure">
    <div class="card bg-warning border-0 rounded-0 pt-5">
        <div class="container">
            <h1 class="text-capitalize pt-lg-3">nossa estrutura</h1>
        </div>
    </div>
    <div class="container">       
        <div class="owl-carousel owl-themer" id="structure-carousel">
            @foreach ($items as $item)
                <div class="item">
                    <div class="card w-100 border-0">
                        <img src="{{ $item->files->path }}" class="card-img-top img-fluid" alt="{{ $item->title }}">
                        <div class="card-body py-5">
                            <h5 class="card-title text-center py-2"><strong>{{ $item->title }}</strong></h5>
                            <p class="card-text">{!! $item->content !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row py-4 py-lg-5 d-flex justify-content-center">
            <div class="structure-number my-5">
                <ul class="nav flex-column align-content-center flex-lg-row justify-content-lg-between py-3">
                    <li class="nav-item d-flex justify-content-center flex-column">
                        <p class="text-center pt-4">
                            <strong>00</strong>
                        </p>
                        <p class="text-center pb-2">
                            UFV's gerados
                        </p>
                    </li>
                    <li class="nav-item d-flex justify-content-center flex-column">
                        <p class="text-center pt-4">
                            <strong>00</strong>
                        </p>
                        <p class="text-center pb-2">
                            Árvores cultivadas
                        </p>
                    </li>
                    <li class="nav-item d-flex justify-content-center flex-column">
                        <p class="text-center pt-4">
                            <strong>00</strong>
                        </p>
                        <p class="text-center pb-2">
                            Carbono evitados
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>