<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Structures extends Component
{
    public function render()
    {
        return view('livewire.structures');
    }
}
