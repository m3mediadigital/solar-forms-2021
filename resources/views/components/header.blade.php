<header>
    <nav class="navbar navbar-expand-lg p-5 position-absolute w-100 d-lg-none">
        <div class="container d-flex justify-content-center">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('images/icons/logo.svg') }}" alt="{{ env('APP_NAME') }}">
            </a>
        </div>
    </nav>
    <nav class="navbar navbar-expand-lg p-5 d-none d-lg-flex position-absolute w-100">
        <div class="container d-flex justify-content-center d-lg-block">
          
            {{--<button class="navbar-toggler d-none" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>--}}
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 w-100 d-flex justify-content-around align-items-center">
                    <li class="nav-item d-flex align-items-center">
                        <a class="navbar-brand" style="margin-bottom: 1.9rem" href="#">
                            <img src="{{ asset('images/icons/logo.svg') }}" alt="{{ env('APP_NAME') }}">
                        </a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#beneficies">Benefícios</a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#calculator">Calculadora</a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#structure">Nossa estrutura</a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#insight">Nossos clientes</a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#blog">Notícias</a>
                    </li>
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white" href="#contact">Contato</a>
                    </li>
                    {{-- <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white btn btn-warning text-uppercase" href="#">acesso cliente</a>
                    </li> --}}
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link text-white btn btn-warning text-uppercase" href="https://rovemaenergia.com.br/simulador-de-energia/ces" style="font-size: 13px">geração distribuida</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="slide">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active d-none d-md-block">
                    <img src="{{ asset('images/min/slide.png') }}" class="d-block img-fluid w-100" alt="...">
                </div>
                <div class="carousel-item active d-md-none">
                    <img src="{{ asset('images/min/slide-mobile.png') }}" class="d-block img-fluid w-100" alt="...">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>
</header>