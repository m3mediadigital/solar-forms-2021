<?php

namespace Database\Seeders;

use App\Models\Files;
use App\Models\Structures;
use Illuminate\Database\Seeder;

class StructuresTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'image' => 'images/min/porto.png',
                'title' => 'UFV Porto Velho',
                'content' => 'Capacidade total instalada 0,35 MWp, suficiente para atender aproximadamente 250 residências (consumo médio de 152 kWh/mês) ou 750 pessoas. 1.000 placas instaladas.'
            ],
            [
                'image' => 'images/min/francisco.png',
                'title' => 'UFV São Francisco',
                'content' => 'Capacidade total instalada 2 MWp, suficiente para atender aproximadamente 2.000 residências (consumo médio de 152 kWh/Mês) ou 6.000 pessoas. 5.200 placas instaladas em área de 60.000m².'
            ],
        ];

        foreach ($items as $item) {
            $file = new Files;
            $file->name = $item['image'];
            $file->filename = $item['image'];
            $file->path = $item['image'];
            $file->save();

            $bene = new Structures;
            $bene->title = $item['title'];
            $bene->content = $item['content'];
            $bene->files_id = $file->id;
            $bene->save();
        }
    }
}
