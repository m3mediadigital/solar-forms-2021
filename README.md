
## About Project

Site da Rovema Energias Renováveis

## Layout

- **[https://xd.adobe.com/view/ada7309e-a2b6-40c2-9497-e8fa5c66374d-f8f4/](https://xd.adobe.com/view/ada7309e-a2b6-40c2-9497-e8fa5c66374d-f8f4/)**
### Pacotes

- **[Laravel](https://laravel.com)**
- **[Argon Dashboard](https://argon-dashboard-laravel.creative-tim.com/docs/getting-started/installation.html)**
- **[Eloquent UUID](https://github.com/goldspecdigital/laravel-eloquent-uuid)**
- **[Eloquent-Sluggable](https://github.com/cviebrock/eloquent-sluggable)**
- **[Blade UIkit](https://blade-ui-kit.com/)**
- **[Livewire](https://laravel-livewire.com/docs/2.x/quickstart)**
- **[Intervention Image](http://image.intervention.io/getting_started/introduction)**
- **[Estados, Municipios, Bairros e Regiões do Brasil](https://github.com/chandez/Estados-Cidades-IBGE)**
- **[Laravel Minify Html](https://packagist.org/packages/workspace/laravel-minify-html)**


