 <footer class="py-5">
    <div class="container footer-text pt-3">
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center d-lg-block pb-3">
                        <img src="{{ asset('argon/img/brand/logo.svg') }}" class="img-fluid" alt="{{ env('APP_NAME') }}">
                    </div>
                    <div class="col-12 col-lg-6 pt-4 text-center text-lg-start">
                        <p><strong>Rovema Energia</strong></p>
                        <p class="m-0"><strong>Departamento Comercial: </strong> (69) 3211-0555</p>
                        <p class="m-0"><strong>Consultor de Cooperativa: </strong> (69) 3211-0555</p>
                        <p class="m-0"><strong>WhatsApp: </strong> (69) 99385-8245</p>
                    </div>
                    <div class="col-12 col-lg-6 pt-4 text-center text-lg-start">
                        <p><strong>E-mail Comercial: </strong>comercial02@rovemaenergia.com.br <br> <span class="text-uppercase">(vendas e orçamentos)</span></p>
                        <p><strong>E-mail Administrativo: </strong>administrativo@ces-ro.com.br <br> <span class="text-uppercase">(termo de adesão)</span></p>
                    </div>
                    <div class="col-12 pt-4 d-flex justify-content-center d-lg-block" id="redes">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="http://" target="_blank" rel="noopener noreferrer" class="nav-link d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/icons/facebook.png') }}" alt="Facebook">
                                </a>
                            </li>
                                <li class="nav-item">
                                <a href="http://" target="_blank" rel="noopener noreferrer" class="nav-link d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/icons/instagram.png') }}" alt="instagram">
                                </a>
                            </li>
                            <li class="nav-item d-flex align-items-center">
                                <a href="http://" target="_blank" rel="noopener noreferrer" class="nav-link d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/icons/youtube.png') }}" alt="Youtube" style="width: 42px;">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 pt-4 d-flex justify-content-center d-lg-block">
                        
                        <p>
                            <a href="https://gruporovema.com.br/politica-de-privacidade" target="_blank">Política de Privacidade</a>
                        </p>
                        <p>
                            <a href="https://portaldotitular.safecomply.com.br/safecomply/gruporovema">Registre uma Solicitação- LGPD</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 d-none d-lg-block">
                <img src="{{ asset('images/min/img-footer.svg') }}" class="img-fluid" alt="">
            </div>
            <div class="row d-none d-md-block pt-5">
                <ul class="nav justify-content-between pt-5">
                    <li class="nav-item" style="font-size: 10px">
                        {{ now()->format('Y') }} - Todos os direitos reservados
                    </li>
                    <li class="nav-item">
                        <img src="{{ asset('images/icons/m3.svg') }}" class="img-fluid" alt="Nova M3">
                    </li>
                    <li class="nav-item">
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-mobile d-flex justify-content-center position-fixed d-lg-none w-100">
        <div class="container">
            <ul class="nav justify-content-between w-100">
                <li class="nav-item">
                    <a class="nav-link py-3" aria-current="page" href="#contact">
                        <img src="{{ asset('images/icons/email.png') }}" alt="Contato">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-3" href="#">
                        <img src="{{ asset('images/icons/whatsapp.png') }}" alt="Whatsapp">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-3" href="#redes">
                        <img src="{{ asset('images/icons/logout.png') }}" alt="Redes">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-3" href="#" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                        <img src="{{ asset('images/icons/bars.png') }}" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>