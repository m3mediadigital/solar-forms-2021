<?php

namespace App\Http\Controllers;

use App\Mail\SendContacts;
use App\Mail\SendNewslleters;
use App\Models\Benefits;
use App\Models\Clients;
use App\Models\Contacts;
use App\Models\Newslleters;
use App\Models\Structures;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller{

    public $mail = "valeria@rovemaenergia.com.br";

    public function __invoke($mail)
    {
        $this->mail = $mail;
    }

    public function index()
    {
        $calculator = [
            [
                'title' => 'calculadora',
                'image' => 'calculator.png'
            ],
            [
                'title' => 'termo de adesão',
                'image' => 'termo.png'
            ],
            [
                'title' => 'contato',
                'image' => 'contact.png'
            ]
        ];

        return view('pages.index',[
            'calculator' => $calculator,
            'clients' => Clients::all(),
            'structures' => Structures::all(),
            'benefits' => Benefits::all()
        ]);
    }

    public function sendContacts(Request $request)
    {
        $contact = new Contacts;
        $contact->setData($contact, $request->all());

        try {
            $contact->save();
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao enviar contato. Por favor tente novamente.');
        } finally {
            Mail::to($this->mail)->send(new SendContacts($contact));
        }

        return back()->with('success', 'Em breve entraremos em contato.');
    }

    public function sendNewslleters(Request $request)
    {
        $contact = new Newslleters;
        $contact->email = $request->email;

        try {
            $contact->save();
        } catch (\Throwable $th) {
            throw $th;
            return back()->with('error','Erro ao enviar contato. Por favor tente novamente.');
        } finally {
            Mail::to($this->mail)->send(new SendNewslleters($contact));
        }

        return redirect()->back()->with('success', 'Em breve entraremos em contato.');

    }

}