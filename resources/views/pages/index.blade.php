<x-Layout>
    <x-slot name="content">
        <x-Beneficies :items="$benefits"/>
        <x-Calculator :items="$calculator"/>
        <x-Structure :items="$structures" />
        <x-Insight :items="$clients"/>
        {{-- <x-Blog /> --}}
        <x-Newsletter />
        <x-Contact />
    </x-slot>
</x-Layout>
