<section class="calculator py-3 py-lg-1" id="calculator">
    <div class="container">
        <h1 class="py-4 py-lg-0 pb-5">Venha ter economia</h1>
        <div class="row pt-lg-5 d-none d-md-flex">
            <div class="col-4 nav-link p-0" role="presentation">
                <button class="border-0 btn-hover bg-transparent w-100 nav-link no-collapse active">
                    <img src="{{ asset('images/min/calculator.png') }}" class="img-fluid w-100" alt="calculadora">
                </button>
                <div class="calculator-hover">
                    <div class="calculator-hover-content w-100 d-flex justify-content-center align-content-center flex-column">
                        <h2 class="text-center text-uppercase text-white pt-5 pb-3">calculadora</h2>
                        <p class="text-center">
                            Realize a sua simulação e faça o seu 
                            orçamento. Temos as melhores vantagens para
                            você economizar na sua conta de luz
                        </p>
                        <div class="d-flex justify-content-center pt-4 pb-5">
                            <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#calculadora" aria-controls="calculadora" aria-expanded="false" aria-label="Toggle navigation">
                                solicitar orçamento
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4 nav-link p-0" role="presentation">
                <button class="border-0 btn-hover bg-transparent w-100 nav-link no-collapse active">
                    <img src="{{ asset('images/min/termo.png') }}" class="img-fluid w-100" alt="termo">
                </button>
                <div class="calculator-hover">
                    <div class="calculator-hover-content w-100 d-flex justify-content-center align-content-center flex-column">
                        <h2 class="text-center text-uppercase text-white pt-5 pb-3">termo de adesão</h2>
                        <p class="text-center">
                            Realize a sua simulação e faça o seu 
                            orçamento. Temos as melhores vantagens para
                            você economizar na sua conta de luz
                        </p>
                        <div class="d-flex justify-content-center pt-4 pb-5">
                            <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#termo-de-adesao" aria-controls="termo-de-adesao" aria-expanded="false" aria-label="Toggle navigation">
                                aderir agora
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4 nav-link p-0" role="presentation">
                <button class="border-0 btn-hover bg-transparent w-100 nav-link no-collapse active">
                    <img src="{{ asset('images/min/contact.png') }}" class="img-fluid w-100" alt="termo">
                </button>
                <div class="calculator-hover">
                    <div class="calculator-hover-content w-100 d-flex justify-content-center align-content-center flex-column">
                        <h2 class="text-center text-uppercase text-white pt-5 pb-3">contato</h2>
                        <p class="text-center">
                            Realize a sua simulação e faça o seu 
                            orçamento. Temos as melhores vantagens para
                            você economizar na sua conta de luz
                        </p>
                        <div class="d-flex justify-content-center pt-4 pb-5">
                            <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#contato" aria-controls="contato" aria-expanded="false" aria-label="Toggle navigation">
                                entre em contato
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-lg-none" id="pills-tab" role="tablist">
             <div class="owl-carousel owl-themer" id="calculator-carousel">

                <div class="item d-flex justify-content-center">
                    <div class="nav-link p-0" role="presentation">
                        <div class="calculator-hover">
                            <div class="calculator-hover-content d-flex justify-content-center align-content-center flex-column">
                                <h2 class="text-uppercase pt-5 text-center">calculadora</h2>
                                <p class="text-center pt-4">
                                    Realize a sua simulação e faça o seu 
                                    orçamento. Temos as melhores vantagens para
                                    você economizar na sua conta de luz
                                </p>
                                <div class="d-flex justify-content-center pt-4 pb-5">
                                    <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#calculadora" aria-controls="calculadora" aria-expanded="false" aria-label="Toggle navigation">
                                        solicitar orçamento
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item d-flex justify-content-center">
                    <div class="nav-link p-0" role="presentation">
                        <div class="calculator-hover">
                            <div class="calculator-hover-content d-flex justify-content-center align-content-center flex-column">
                                <h2 class="text-uppercase pt-5 text-center">termo de adesão</h2>
                                <p class="text-center pt-4">
                                    Realize a sua simulação e faça o seu 
                                    orçamento. Temos as melhores vantagens para
                                    você economizar na sua conta de luz
                                </p>
                                <div class="d-flex justify-content-center pt-4 pb-5">
                                    <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#termo-de-adesao" aria-controls="termo-de-adesao" aria-expanded="false" aria-label="Toggle navigation">
                                        aderir agora
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item d-flex justify-content-center">
                    <div class="nav-link p-0" role="presentation">
                        <div class="calculator-hover">
                            <div class="calculator-hover-content d-flex justify-content-center align-content-center flex-column">
                                <h2 class="text-uppercase pt-5 text-center">contato</h2>
                                <p class="text-center pt-4">
                                    Realize a sua simulação e faça o seu 
                                    orçamento. Temos as melhores vantagens para
                                    você economizar na sua conta de luz
                                </p>
                                <div class="d-flex justify-content-center pt-4 pb-5">
                                    <button class="btn btn-warning text-uppercase no-collapse" type="button" data-bs-toggle="collapse" data-bs-target="#contato" aria-controls="contato" aria-expanded="false" aria-label="Toggle navigation">
                                        entre em contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>

        <div class="collapse py-5" id="calculadora">            
            <h2>Caluladora CES</h2>
            <p class="py-3 py-md-5">
                Realize a sua simulação e faça o seu orçamento. Temos as melhores vantagens para <br>
                você economizar na sua conta de luz
            </p>

            <div class="row">
                <div class="col-12 col-md-6 d-flex justify-content-center py-2 py-lg-3">
                    <a href="https://rovemaenergia.com.br/simulador-de-energia/calculamos-para-voce" class="btn text-uppercase text-center w-100 py-2 py-lg-2" target="_blank" rel="noopener noreferrer">
                        Envie sua fatura
                    </a>
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center py-2 py-lg-3">
                    <a href="https://rovemaenergia.com.br/simulador-de-energia/ces" class="btn text-uppercase text-center w-100 py-2 py-lg-2" target="_blank" rel="noopener noreferrer">
                        Iniciar Simulação
                    </a>
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center py-2 py-lg-3">
                    <a href="https://rovemaenergia.com.br/simulador-de-energia/cooperativa" class="btn text-uppercase text-center w-100 py-2 py-lg-2" target="_blank" rel="noopener noreferrer">
                        Cooperativa de energia solar
                    </a>
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center py-2 py-lg-3">
                    <a href="https://rovemaenergia.com.br/simulador-de-energia/beneficios-ces" class="btn text-uppercase text-center w-100 py-2 py-lg-2" target="_blank" rel="noopener noreferrer">
                        benefícios ces
                    </a>
                </div>
            </div>
        </div>

        <div  class="collapse py-5 collapsed" id="termo-de-adesao">
            <h2>Termo de Adesão</h2>
            <form action="">
                <div class="row g-3">
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="text" class="form-control border-0 border-bottom text-uppercase" id="floatingInput" name="unit_consul" placeholder="informe sua unidade consumidora (uc)" aria-label="informe sua unidade consumidora (uc)">
                        <label for="floatingInput" class="text-uppercase">informe sua unidade consumidora (uc)</label>
                        <x-error field="unit_consul" class="text-danger" />
                    </div>
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="text" class="form-control border-0 border-bottom text-uppercase" id="cpf" name="cpf" placeholder="cpf do titular da unidade consumidora (uc)" aria-label="cpf do titular da unidade consumidora (uc)">
                        <label for="cpf" class="text-uppercase">cpf do titular da unidade consumidora (uc)</label>
                        <x-error field="cpf" class="text-danger" />
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="email" class="form-control border-0 border-bottom text-uppercase" id="email" name="email" placeholder="digite endereço da unidade consumidora (uc)" aria-label="digite endereço da unidade consumidora (uc)">
                        <label for="adrress" class="text-uppercase">digite endereço da unidade consumidora (uc)</label>
                        <x-error field="address" class="text-danger" />
                    </div>
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="email" class="form-control border-0 border-bottom text-uppercase" id="email" name="email" placeholder="email para envio do termo de adesão" aria-label="email para envio do termo de adesão">
                        <label for="email" class="text-uppercase">email para envio do termo de adesão</label>
                        <x-error field="email" class="text-danger" />
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="tel" class="form-control border-0 border-bottom text-uppercase" id="phone" name="phone" placeholder="telefone para contato" aria-label="telefone para contato">
                        <label for="phone" class="text-uppercase">telefone para contato</label>
                        <x-error field="phone" class="text-danger" />
                    </div>
                </div>
                <div class="w-100 d-flex justify-content-center py-3">
                    <button type="submit" class="btn btn-warning py-2 py-lg-3 px-3 px-lg-5 text-uppercase">enviar</button>
                </div>
            </form>
        </div>

        <div  class="collapse py-5 collapsed" id="contato">
            <h2>Contato</h2>
            <form action="{{ route('sendContacts') }}" method="post">
                @csrf
                <div class="row g-3">
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="text" class="form-control border-0 border-bottom" id="floatingInput" name="name" placeholder="Nome" aria-label="Nome">
                        <label for="floatingInput">Nome</label>
                        <x-error field="name" class="text-danger" />
                    </div>
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="tel" class="form-control border-0 border-bottom" id="whatsapp" name="phone" placeholder="Whatsapp" aria-label="Whatsapp">
                        <label for="whatsapp">Whatsapp</label>
                        <x-error field="cpf" class="text-danger" />
                    </div>
                </div>
                <div class="row g-3">
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="text" class="form-control border-0 border-bottom" id="adrress" name="email" placeholder="E-mail" aria-label="E-mail">
                        <label for="email">E-mail</label>
                        <x-error field="email" class="text-danger" />
                    </div>
                    <div class="col-12 col-lg-6 form-floating py-3">
                        <input type="text" class="form-control border-0 border-bottom" id="observe" name="message" placeholder="Observação" aria-label="Observação">
                        <label for="observe">Observação</label>
                        <x-error field="observe" class="text-danger" />
                    </div>
                </div>
                <div class="w-100 d-flex justify-content-center py-3">
                    <button type="submit" class="btn btn-warning py-2 px-5 text-uppercase">enviar</button>
                </div>
            </form>
        </div>
    </div>
</section>

<x-slot name="js">
    <script>
        $(document).ready(() => {
            $(".no-collapse").click(function () {
                $(".calculator .collapse").collapse("hide");
            });
        })
    </script>
</x-slot>