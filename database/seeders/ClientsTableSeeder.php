<?php

namespace Database\Seeders;

use App\Models\Clients;
use App\Models\Files;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'image' => 'images/min/mapplebear.png',
                'title' => 'Mapple Bear'
            ],
            [
                'image' => 'images/min/aphaclin.png',
                'title' => 'Alphaclin'
            ],
            [
                'image' => 'images/min/cna.png',
                'title' => 'CNA Ingles Definitivo'
            ]
        ];

        foreach ($items as $item) {
            $file = new Files;
            $file->name = $item['image'];
            $file->filename = $item['image'];
            $file->path = $item['image'];
            $file->save();

            $cli = new Clients;
            $cli->title = $item['title'];
            $cli->files_id = $file->id;
            $cli->save();
        }
    }
}
