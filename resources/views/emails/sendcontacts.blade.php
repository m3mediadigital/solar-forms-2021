@component('mail::message')
# Introduction

Novo contato chegou

@component('mail::button', ['url' => ''])
Clique aqui
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
