<section class="beneficies py-3 py-lg-5" id="beneficies">
    <img src="{{ asset('images/icons/bene-left.svg') }}" class="beneficies-left img-position position-absolute d-none d-lg-block ps-xl-3">
    <div class="container py-5">
        <h1 class="text-capitalize pb-lg-5">benefícios</h1>
        <ul class="nav justify-content-lg-between">
            @foreach ($items as $item)
                <li class="nav-item d-flex align-items-center py-3">
                    <img src="{{ $item->files->path }}" alt="Lorem">
                    <p class="ps-3 m-0">{{ $item->title }}</p>
                </li>
            @endforeach
        </ul>
    </div>
    <img src="{{ asset('images/icons/bene-right.svg') }}" class="beneficies-right img-position position-absolute d-none d-xl-block ">
</section>